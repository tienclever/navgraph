package com.example.navgraph.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.example.navgraph.R
import com.example.navgraph.model.User
import kotlinx.android.synthetic.main.fragment_game.btn_login
import kotlinx.android.synthetic.main.fragment_game.et_name
import kotlinx.android.synthetic.main.fragment_game.et_password
import kotlinx.android.synthetic.main.fragment_game.tvData

class GameFragment : Fragment() {

    val args: GameFragmentArgs by navArgs()
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_game, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        tvData.text = args.sentGame.toString()

        btn_login.setOnClickListener {
            val name = et_name.text.toString()
            val password = et_password.text.toString()

            val user = User(name, password)

            val action = GameFragmentDirections.actionGameFragmentToUserFragment(user)
            findNavController().navigate(action)
        }
    }
}