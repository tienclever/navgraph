package com.example.navgraph.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.example.navgraph.R
import kotlinx.android.synthetic.main.fragment_home.start_game_frag
import kotlinx.android.synthetic.main.fragment_home.start_music_frag

class HomeFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        start_game_frag.setOnClickListener {
            val actionGame = HomeFragmentDirections.actionToGameFragment(2)
            findNavController().navigate(actionGame)
        }
        start_music_frag.setOnClickListener {
            val actionMusic = HomeFragmentDirections.actionToMusicFragment(1)
            findNavController().navigate(actionMusic)
        }
    }
}