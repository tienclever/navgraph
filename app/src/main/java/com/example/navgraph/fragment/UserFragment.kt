package com.example.navgraph.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.navArgs
import com.example.navgraph.R
import kotlinx.android.synthetic.main.fragment_user.tv_data

class UserFragment : Fragment() {
    val args: UserFragmentArgs by navArgs()
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_user, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val nameArgs = args.gameUser.name
        val passwordArgs = args.gameUser.password
        tv_data.text = "name: ${nameArgs} \npassword: ${passwordArgs}"
    }
}